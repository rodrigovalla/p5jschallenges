/*
Esto es un comentario y no es parte del código.
Primero vamos a definir una variable llamada "pelotas".
En esta variable vamos a guardar una colección de objetos que
representan una pelota.
*/
let pelotas;
/*
La biblioteca p5js tiene dos funciones que casi
siempre tienen que estar. Una es la función setup(). Esta
función se ejecuta una sóla vez al cargar la página en el
navegador.
*/
function setup() {
	createCanvas(windowWidth, windowHeight); //creamos un lienzo
	background(25);
	frameRate(60);
	pelotas = [];
	//ahora vamos a crear unas cuantas pelotas para usar diciéndole un color, un tamaño y la posición
	for (let i=0; i < 11; i++){
		pelotas.push(new ball(color(100,150,175), 40, random(width/2) + width/2, random(height), 0.95));
	}
	print("Hola, estás programando en javascript"); //imprimimos un mensaje en la consola
}

/*
La otra función es la función draw(). Esta función
se ejecuta muchas veces por segundo (la computadora intenta que
sean 60 pero a veces no llega).
*/
function draw() {
	background(25,25,25,125);
	fill(200);
	actualizarPelotas();
	acelerarPelotas();
	//pelotas[0].speed(mouseX-pmouseX, mouseY-pmouseY);
	//pelotas[0].move(mouseX, mouseY);
	dibujarPelotas();
	chequearParedes(); //tenemos que hacer que no se escape de la pantalla
}

function actualizarPelotas(){
	for (let i=0; i<pelotas.length; i++){
		pelotas[i].update();
	}
}

function dibujarPelotas(){
	for (let i=0; i<pelotas.length; i++){
		pelotas[i].display();
	}
}

function acelerarPelotas(){
	for (let i=0; i<pelotas.length; i++){
		for (let j=1; j<pelotas.length; j++){
			if (dist(pelotas[i].x,pelotas[i].y,pelotas[j].x,pelotas[j].y) < 40){
				pelotas[i].accelerate(-pelotas[j].dx/10, -pelotas[j].dy/10);
				pelotas[j].accelerate(pelotas[i].dx/10, pelotas[i].dy/10);
			}
		}
	}
}

function chequearParedes(){
	for (let i=0; i<pelotas.length; i++){
		if (pelotas[i].x < 20 || pelotas[i].x > width - 20) {
			//para que la pelota rebote si se choca a la izquierda o la derecha hay que escribir:
			pelotas[i].bounceX();
		}
		if (pelotas[i].y < 20 || pelotas[i].y > height - 20) {
			//¿Qué habrá que escribir para que rebote si se choca arriba o abajo?
			pelotas[i].bounceY();
		}
	}
}

/*
La función mousePressed se ejecuta cuando hacemos click con el mouse
sobre el lienzo.
*/
function mousePressed() {
	
}

/*
La función keyPressed se ejecuta cuando tocamos alguna tecla en el teclado. Hace
falta indicar qué hacer según qué tecla tocamos.
*/
function keyPressed() {
	if (key === "r"){ //¡Esto es un condicional!
		pelotas[0].reset();
	} else if (key === "d"){
		pelotas[0].accelerate((pelotas[0]-mouseX)/100, (pelotas[0]-mouseY)/100);
	}
}

/*
DESAFÍO 7,1
Este programa dibuja muchas pelotas.
*/
