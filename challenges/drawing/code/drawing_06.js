//Esta función es obligatoria. ¡No tocar!
function setup() {
	createCanvas(600, 600);
	startConfig(25, 4, color(255), color(25));
	noLoop();
	print("Algorithmic drawing v0.6");
	actualizarPantalla();
}

//Esta función que sigue es la que actualiza la pantalla.
//¡Acá tienen que escribir!
//Recuerden que pueden usar las funciones primitivas (dibujar(l),
//saltar(l) y girar(a)) y las otras también.
function actualizarPantalla(){
	prepararHoja();
    //Ya están listas todas las funciones.
	//Es momento de que se luzcan ustedes.
	//Dibujen algo espectacular.
	

}

//Para dibujar un rectángulo.
function rectangulo(l,a){
	for (let i=0; i<2; i++){
      dibujar(l);
	  girar(2);
	  dibujar(a);
	  girar(2); 
    }
}

//Para dibujar una escalera.
function escalera(e,l){
	for (let i=0; i<e; i++){
      dibujar(l);
	  girar(2);
	  dibujar(l);
	  girar(6);	  
    }
}

//Para dibujar un pico.
function pico(l){
	dibujar(l);
	girar(7);
	dibujar(l);
	girar(2);
	dibujar(l);
	girar(7);
	dibujar(l);
}

//Para dibujar un peine.
function peine(d,l){
  for (let i=0; i<d; i++){
	dibujar(l);
	girar(3);
	dibujar(l);
	girar(5);
  }
}

//Para dibujar un rulo.
function rulo(l){
	dibujar(l*2);
	girar(6);
	dibujar(l);
	girar(6);
	dibujar(l);
	girar(6);
	dibujar(l*2);
	girar(6);
}

//Para dibujar un cuadrado.
function cuadrado(c,l){
	for (let i=0; i<c; i++){
      dibujar(l);
	  girar(2);
    }
}

//Para dibujar un octógono.
function octogono(o,l){
  for (let i=0; i<o; i++){
	dibujar(l);
	girar(1);
  }
}

//Esta función se ejecuta cuando hacen click con el mouse.
function mousePressed(){
	origin = [mouseX, mouseY];
	currentdirection = 0;
    actualizarPantalla();
}

//Esta función es para poder guardar con el teclado.
function keyPressed(){
  if (keyCode === ENTER){
	saveCanvas("algorithmicdrawing.jpg")
  }
}