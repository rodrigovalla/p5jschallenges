//Esta función es obligatoria. ¡No tocar!
function setup() {
	createCanvas(600, 600);
	startConfig(25, 4, color(255), color(25));
	noLoop();
	print("Algorithmic drawing v0.6");
	actualizarPantalla();
}

//Esta función que sigue es la que actualiza la pantalla.
//¡Acá tienen que escribir!
//Recuerden que pueden usar las funciones primitivas (dibujar(l),
//saltar(l) y girar(a)) y la función rectangulo(l, a).
function actualizarPantalla(){
	prepararHoja();
    //Acá tienen que completar la función para dibujar.
    //¡Hay un problema! Queremos dibujar varios rectángulos 
	//cruzados de distintos tamaños. Pero parece que
	//la función rectangulo(l, a) está mal.
	//Tienen que corregirla más abajo.
	rectangulo(2,4);
	rectangulo(4,2);
	rectangulo(3,5);
	rectangulo(5,3);
	rectangulo(6,9);
	rectangulo(9,6);


}

//Para dibujar un rectángulo.
function rectangulo(l,a){
	for (let i=0; i<2; i++){
	  //Acá hay un problema. Las letras l y a no se están
	  //usando. Corrijan eso please...
      dibujar(3);
	  girar(2);
	  dibujar(2);
	  girar(2); 
    }
}

//Esta función se ejecuta cuando hacen click con el mouse.
function mousePressed(){
	origin = [mouseX, mouseY];
	currentdirection = 0;
    actualizarPantalla();
}

//Esta función es para poder guardar con el teclado.
function keyPressed(){
  if (keyCode === ENTER){
	saveCanvas("algorithmicdrawing.jpg")
  }
}