//Esta función es obligatoria. ¡No tocar!
function setup() {
	createCanvas(600, 600);
	startConfig(25, 4, color(255), color(25));
	noLoop();
	print("Algorithmic drawing v0.6");
	actualizarPantalla();
}

//Esta función que sigue es la que actualiza la pantalla.
//¡Acá tienen que escribir!
//Recuerden que pueden usar las funciones primitivas (dibujar(l),
//saltar(l) y girar(a)) y las otras también.
function actualizarPantalla(){
	prepararHoja();
    //Acá tienen que completar la función para dibujar.
    //Me gustaría dibujar una figura cerrada escalonada.
	//¡Pero la función escalera está completa!
	//Corrijan primero la función escalera y después
	//completen esta parte para que la figura se cierre.
	escalera(4,2);
	girar(2);
	escalera(8,1);



}

//Para dibujar un rectángulo.
function rectangulo(l,a){
	for (let i=0; i<2; i++){
      dibujar(l);
	  girar(2);
	  dibujar(a);
	  girar(2); 
    }
}

//Para dibujar una escalera.
function escalera(e,l){
	for (let i=0; i<e; i++){
      //Me olvidé completamente como se dibuja un escalón.
	  //Eso es lo que hace falta acá.
	  dibujar(l);



    }
}

//Esta función se ejecuta cuando hacen click con el mouse.
function mousePressed(){
	origin = [mouseX, mouseY];
	currentdirection = 0;
    actualizarPantalla();
}

//Esta función es para poder guardar con el teclado.
function keyPressed(){
  if (keyCode === ENTER){
	saveCanvas("algorithmicdrawing.jpg")
  }
}