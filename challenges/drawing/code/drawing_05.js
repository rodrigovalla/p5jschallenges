//Esta función es obligatoria. ¡No tocar!
function setup() {
	createCanvas(600, 600);
	startConfig(25, 4, color(255), color(25));
	noLoop();
	print("Algorithmic drawing v0.6");
	actualizarPantalla();
}

//Esta función que sigue es la que actualiza la pantalla.
//¡Acá tienen que escribir!
//Recuerden que pueden usar las funciones primitivas (dibujar(l),
//saltar(l) y girar(a)) y las otras también.
function actualizarPantalla(){
	prepararHoja();
    //Acá tienen que completar la función para dibujar.
    //Una vez más queremos dibujar una figura cerrada.
	//No hay ningún problema con el código de aquí, el
	//problema está en la función rulo(l) que por el
	//momento no hace nada. ¡Corríjanla!
	rulo(1);
	rulo(2);
	rulo(1);
	rulo(2);

}

//Para dibujar un rectángulo.
function rectangulo(l,a){
	for (let i=0; i<2; i++){
      dibujar(l);
	  girar(2);
	  dibujar(a);
	  girar(2); 
    }
}

//Para dibujar una escalera.
function escalera(e,l){
	for (let i=0; i<e; i++){
      dibujar(l);
	  girar(2);
	  dibujar(l);
	  girar(6);	  
    }
}

//Para dibujar un pico.
function pico(l){
	dibujar(l);
	girar(7);
	dibujar(l);
	girar(2);
	dibujar(l);
	girar(7);
	dibujar(l);
}

//Para dibujar un peine.
function peine(d,l){
  for (let i=0; i<d; i++){
	dibujar(l);
	girar(3);
	dibujar(l);
	girar(5);
  }
}

//Para dibujar un rulo.
function rulo(l){
	//Aquí tienen que decidir qué demonios hace el rulo...
	

	
}

//Esta función se ejecuta cuando hacen click con el mouse.
function mousePressed(){
	origin = [mouseX, mouseY];
	currentdirection = 0;
    actualizarPantalla();
}

//Esta función es para poder guardar con el teclado.
function keyPressed(){
  if (keyCode === ENTER){
	saveCanvas("algorithmicdrawing.jpg")
  }
}