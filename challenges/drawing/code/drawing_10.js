//Esta función es obligatoria. ¡No tocar!
function setup() {
	createCanvas(600, 600);
	startConfig(20, 4, color(255), color(25));
	noLoop();
	print("Algorithmic drawing v0.6");
	actualizarPantalla();
}

//Esta función que sigue es la que actualiza la pantalla.
//¡Acá tienen que escribir!
//Recuerden que pueden usar las funciones primitivas (dibujar(l),
//saltar(l) y girar(a)) y las otras también (rectangulo(l,a),
//escalera(e,l), pico(l), peine(d,l), rulo(l), cuadrado(c,l)
//y octogono(o,l)).
function actualizarPantalla(){
	prepararHoja();
    //Un bucle puede estar adentro de otro bucle y de otro bucle...
    for (let h=0; h<4; h++){
        for (let i=0; i<2; i++){
            for (let j=0; j<4; j++){
                //He aquí un nuevo blucle. No podíamos volver a usar "i".
                octogono(8,1);
                peine(3,1);
                girar(2);
            }
            //Ahora tienen que agregar algo para que el patrón
            //del bucle interno se repita 7 cuadraditos a la derecha.


        }
        //Finalmente agreguen acá, en el bucle de más afuera,
        //lo que necesiten para que el patrón se repita describiendo
        //un cuadrado.

        

    }
        

}

//Para dibujar un rectángulo.
function rectangulo(l,a){
	for (let i=0; i<2; i++){
      dibujar(l);
	  girar(2);
	  dibujar(a);
	  girar(2); 
    }
}

//Para dibujar una escalera.
function escalera(e,l){
	for (let i=0; i<e; i++){
      dibujar(l);
	  girar(2);
	  dibujar(l);
	  girar(6);	  
    }
}

//Para dibujar un pico.
function pico(l){
	dibujar(l);
	girar(7);
	dibujar(l);
	girar(2);
	dibujar(l);
	girar(7);
	dibujar(l);
}

//Para dibujar un peine.
function peine(d,l){
  for (let i=0; i<d; i++){
	dibujar(l);
	girar(3);
	dibujar(l);
	girar(5);
  }
}

//Para dibujar un rulo.
function rulo(l){
	dibujar(l*2);
	girar(6);
	dibujar(l);
	girar(6);
	dibujar(l);
	girar(6);
	dibujar(l*2);
	girar(6);
}

//Para dibujar un cuadrado.
function cuadrado(c,l){
	for (let i=0; i<c; i++){
      dibujar(l);
	  girar(2);
    }
}

//Para dibujar un octógono.
function octogono(o,l){
  for (let i=0; i<o; i++){
	dibujar(l);
	girar(1);
  }
}

//Esta función se ejecuta cuando hacen click con el mouse.
function mousePressed(){
	origin = [mouseX, mouseY];
	currentdirection = 0;
    actualizarPantalla();
}

//Esta función es para poder guardar con el teclado.
function keyPressed(){
  if (keyCode === ENTER){
	saveCanvas("algorithmicdrawing.jpg")
  }
}