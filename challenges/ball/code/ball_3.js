/*
Esto es un comentario y no es parte del código.
Primero vamos a definir una variable llamada "pelota".
Además vamos a tener las variables "potencia" y "puntos".
En esta variable vamos a guardar un objeto que representa una pelota.
*/
let pelota;
let potencia;
let puntos;
/*
La biblioteca p5js tiene dos funciones casi que
siempre tienen que estar. Una es la función setup(). Esta
función se ejecuta una sóla vez al cargar la página en el
navegador.
*/
function setup() {
	createCanvas(windowWidth, windowHeight); //creamos un lienzo
	background(25,25,25,125);
	frameRate(60);
	//ahora vamos a crear una pelota para usar diciéndole un color, un tamaño y la posición
	pelota = new ball(color(200,100,50), 40, 100, height/2, 0.96);
	potencia = 10; //la potencia del disparo
	puntos = 0; //los puntos que hayamos ganado
	textAlign(TOP, TOP);
	textSize(height / 25);
	print("Hola, estás programando en javascript"); //imprimimos un mensaje en la consola
}

/*
La otra función es la función draw(). Esta función
se ejecuta muchas veces por segundo (la computadora intenta que
sean 60 pero a veces no llega).
*/
function draw() {
	background(25,25,25,125);
	fill(50,100,200);
	pelota.update(); //antes que nada actualizamos la pelota
	if (abs(pelota.x - (width - 200)) < 40 & pelota.y > (mouseY-100) & pelota.y < (mouseY+100)){
		pelota.accelerate(-50, 0); //aceleramos la pelota cuando rebota

	}
	rect(width-200,mouseY-100,50,200);
	pelota.display(); //dibujamos la pelota (ella sabe a donde está)
	chequearParedes(); //tenemos que hacer que no se escape de la pantalla
	fill(200);
	text("Puntaje: " + puntos, 10, 10);
}

function chequearParedes(){
	if (pelota.x < 20) {
		pelota.bounceX();
	} else if (pelota.x > width - 20){
		pelota.reset();
		puntos += 1;
	}
	if (pelota.y < 20 || pelota.y > height - 20) {
		pelota.bounceY();
	}
}

/*
La función mousePressed se ejecuta cuando hacemos click con el mouse
sobre el lienzo.
*/
function mousePressed() {
	
}

/*
La función keyPressed se ejecuta cuando tocamos alguna tecla en el teclado. Hace
falta indicar qué hacer según qué tecla tocamos.
*/
function keyPressed() {
	if (key === "d" || key === "D"){
		pelota.accelerate(potencia,random(potencia)-potencia/2);
	} else if (key === "r" || key === "R"){ //¡Esto es un condicional!
		pelota.reset();
	} else if (key === "+"){
		potencia += 5;
	}
}

/*
DESAFÍO 5.3
Este programa dibuja una pelota. El mouse mueve un rectángulo que busca
evitar que la pelota llegue al borde. Con la tecla "d" realizan un disparo.
Tienen que conseguir anotar dos puntajes, la cantidad de goles y la 
cantidad de atajadas.
*/
