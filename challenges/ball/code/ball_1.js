/*
Esto es un comentario y no es parte del código.
Primero vamos a definir una variable llamada "pelota".
En esta variable vamos a guardar un objeto que representa una pelota.
*/
let pelota;
/*
La biblioteca p5js tiene dos funciones que casi
siempre tienen que estar. Una es la función setup(). Esta
función se ejecuta una sóla vez al cargar la página en el
navegador.
*/
function setup() {
	createCanvas(windowWidth, windowHeight); //creamos un lienzo
	background(25);
	frameRate(60);
	//ahora vamos a crear una pelota para usar diciéndole un color, un tamaño y la posición
	pelota = new ball(color(200), 40, random(width), random(height), 0.96);
	print("Hola, estás programando en javascript"); //imprimimos un mensaje en la consola
}

/*
La otra función es la función draw(). Esta función
se ejecuta muchas veces por segundo (la computadora intenta que
sean 60 pero a veces no llega).
*/
function draw() {
	background(25,25,25,125);
	fill(200);
	pelota.update(); //antes que nada actualizamos la pelota
	if (dist(mouseX,mouseY,pelota.x,pelota.y) < 40){
		pelota.accelerate(mouseX - pmouseX, mouseY - pmouseY); //aceleramos la pelota cuando le pegamos
	}
	ellipse(mouseX,mouseY,40,40);
	pelota.display(); //dibujamos la pelota (ella sabe a donde está)
	chequearParedes(); //tenemos que hacer que no se escape de la pantalla
}

function chequearParedes(){
	if (pelota.x < 20 || pelota.x > width - 20) {
		//para que la pelota rebote si se choca a la izquierda o la derecha hay que escribir:
		pelota.bounceX();
	}
	if (pelota.y < 20 || pelota.y > height - 20) {
		//¿Qué habrá que escribir para que rebote si se choca arriba o abajo?
		
	}
}

/*
La función mousePressed se ejecuta cuando hacemos click con el mouse
sobre el lienzo.
*/
function mousePressed() {
	
}

/*
La función keyPressed se ejecuta cuando tocamos alguna tecla en el teclado. Hace
falta indicar qué hacer según qué tecla tocamos.
*/
function keyPressed() {
	if (key === "r"){ //¡Esto es un condicional!
		pelota.reset();
	}
}

/*
DESAFÍO 5,1
Este programa dibuja una pelota. En la posición del mouse hay otra pelota.
Si las pelotas se chocan, la pelota impactada acelera y se mueve.
Tienen que lograr que la pelota permanezca en la pantalla y que la pelota
y la bola del mouse sean de distinto color.
*/
