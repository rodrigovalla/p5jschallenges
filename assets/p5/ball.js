class ball {

  constructor(c, d, x, y, f){
    this.c = c;
    this.d = d;
    this.x = x;
    this.y = y;
    this.f = f;
    this.dx = 0;
    this.dy = 0;
  }

  display(){
    fill(this.c);
    noStroke();
    ellipse(this.x, this.y, this.d, this.d);
  }

  update(){
    this.brake();
    this.x += this.dx;
    this.y += this.dy;
  }

  speed(x, y){
    this.dx = x;
    this.dy = y; 
  }

  brake(){
    this.dx = this.dx * this.f;
    this.dy = this.dy * this.f;
  }

  bounce(){
    this.dx = this.dx * -1;
    this.dy = this.dy * -1;
  }

  bounceX(){
    this.dx = this.dx * -1;
  }

  bounceY(){
    this.dy = this.dy * -1;
  }

	move(x, y){
		this.x = x;
		this.y = y;
	}

  accelerate(ax, ay){
    this.dx += ax;
    this.dy += ay;
  }

  reset(){
    this.x = windowWidth / 2;
    this.y = windowHeight / 2;
    this.dx = 0;
    this.dy = 0;
  }

}
