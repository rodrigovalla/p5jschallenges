let lw, step, textline;
let c, bg;
let thecanvas, showinfo, showgrid;
let origin, lastpoint;
let dna, sizes, iterations, currentiterations, currentsize, direction, currentdirection, directions, wheel;

function setup() {
	createCanvas(windowWidth, windowHeight);
	thecanvas = document.getElementsByTagName("canvas")[0];
	thecanvas.addEventListener("mousedown", processEv, false);
	config = getURLParams();
	startConfig(config);
	noLoop();
	print("Algorithmic drawing v0.6");
	getScreen();
}

function getScreen(){
	print(c);
	print(bg);
	background(bg);
	if (showinfo){
		printInfo();
	}
	if (showgrid){
		printGrid();
	}
	lastpoint = origin;
	buildDrawing();
	print("ADN: " + dna);
	print("Direcciones: " + directions);
	print("Tamaños: " + sizes);
	print("Repeticiones: " + iterations);
}

function printInfo(){
	noStroke();
	fill(c);
	text("Repeticiones: " + str(currentiterations), 25, 25);
	text("Dirección: " + str(direction), 25, 25 + textline);
	text("Tamaño: " + str(currentsize), 25, 25 + textline * 2);
}

function printGrid(){
	stroke(c);
	strokeWeight(1);
	line(origin[0],0,origin[0],height);
	let x = origin[0] + step;
	while (x < width){
		line(x,0,x,height);
		x += step;
	}
	x = origin[0] - step;
	while (x > 0){
		line(x,0,x,height);
		x -= step;
	}
	line(0,origin[1],width,origin[1]);
	let y = origin[1] + step;
	while (y < height){
		line(0,y,width,y);
		y += step;
	}
	y = origin[1] - step;
	while (y > 0){
		line(0,y,width,y);
		y -= step;
	}
	strokeWeight(lw*3);
	point(origin[0],origin[1]);
}

function buildDrawing(){
	for (let i = 0; i < dna.length; i++){
		let b = dna[i];
		let l = Number(sizes[i]);
		currentdirection = Number(directions[i]);
		let r = Number(iterations[i]);
		for (let j = 0; j < r; j++){
			switch (b) {
				case "D":
					drawing(l);
					break;
				case "S":
					aStep(l);
					break;
				case "P":
					aPike(l);
					break;
				case "T":
					aTooth(l);
					break;
				case "R":
					aRoller(l);
					break;
				case "J":
					aJump(l);
					break;
				case "4":
					aSquare(l);
					break;
				case "8":
					anOctagon(l);
					break;
			}
		}
	}
}

function getPoint(p, d, l){
	p2 = [];
	p2[0] = p[0] + wheel[d][0] * step * l;
	p2[1] = p[1] + wheel[d][1] * step * l;
	return p2;
}

function drawing(l){
	stroke(c);
	strokeWeight(lw);
	p2 = getPoint(lastpoint, currentdirection, l);
	line(lastpoint[0],lastpoint[1],p2[0],p2[1]);
	lastpoint = p2;
}

function aJump(l){
	p2 = getPoint(lastpoint, currentdirection, l);
	lastpoint = p2;
}

function turn(t){
	currentdirection = (currentdirection + t)%8;
}

function aStep(l){
	drawing(l);
	turn(2);
	drawing(l);
	turn(6);
}

function aPike(l){
	drawing(l);
	turn(7);
	drawing(l);
	turn(2);
	drawing(l);
	turn(7);
	drawing(l);
}

function aTooth(l){
	drawing(l);
	turn(3);
	drawing(l);
	turn(5);
}

function aRoller(l){
	drawing(l*2);
	turn(6);
	drawing(l);
	turn(6);
	drawing(l);
	turn(6);
	drawing(l*2);
	turn(6);
}

function aSquare(l){
	drawing(l);
	turn(2);
}

function anOctagon(l){
	drawing(l);
	turn(1);
}

function keyPressed() {
	if (key === "D" || key === "d") {
		dna += "D";
		sizes += str(currentsize);
		directions += str(direction);
		iterations += str(currentiterations);
	} else if (key === "S" || key === "s") {
		dna += "S";
		sizes += str(currentsize);
		directions += str(direction);
		iterations += str(currentiterations);
	} else if (key === "P" || key === "p") {
		dna += "P";
		sizes += str(currentsize);
		directions += str(direction);
		iterations += str(currentiterations);
	} else if (key === "T" || key === "t") {
		dna += "T";
		sizes += str(currentsize);
		directions += str(direction);
		iterations += str(currentiterations);
	} else if (key === "R" || key === "r") {
		dna += "R";
		sizes += str(currentsize);
		directions += str(direction);
		iterations += str(currentiterations);
	} else if (key === "J" || key === "j") {
		dna += "J";
		sizes += str(currentsize);
		directions += str(direction);
		iterations += str(currentiterations);
	} else if (key === "4") {
		dna += "4";
		sizes += str(currentsize);
		directions += str(direction);
		iterations += str(currentiterations);
	} else if (key === "8") {
		dna += "8";
		sizes += str(currentsize);
		directions += str(direction);
		iterations += str(currentiterations);
	} else if (key === "+") {
		currentsize = (currentsize + 1)%10;
	} else if (key === "-") {
		let m = (currentsize - 1)%10;
		if (m < 0){
			m += 10;
		}
		currentsize = m;
	} else if (keyCode === UP_ARROW) {
		currentiterations = (currentiterations + 1)%10;
	} else if (keyCode === DOWN_ARROW) {
		let m = (currentiterations - 1)%8;
		if (m < 0){
			m += 8;
		}
		currentiterations = m;
	} else if (keyCode === RIGHT_ARROW) {
		direction = (direction + 1)%8;
	} else if (keyCode === LEFT_ARROW) {
		let m = (direction - 1)%8;
		if (m < 0){
			m += 8;
		}
		direction = m;
	} else if (keyCode === BACKSPACE){
		l = dna.length - 1;
		dna = dna.substring(0, l);
		sizes = sizes.substring(0, l);
		directions = directions.substring(0, l);
		iterations = iterations.substring(0, l);
	}  else if (keyCode === DELETE){
		dna = "-";
		sizes = "-";
		directions = "-";
		iterations = "-";
	} else if (keyCode === ENTER){
		saveCanvas("algorithmicdrawing.jpg")
	}
	getScreen();
}

function processEv(){
	origin = [mouseX, mouseY];
	getScreen();
}

function startConfig(config) {
	origin = [width * 0.33, height * 0.66];
	lastpoint = origin;
	currentdirection = 0;
	direction = 0;
	currentiterations = 1;
	wheel = [[0,-1],[1,-1],[1,0],[1,1],[0,1],[-1,1],[-1,0],[-1,-1]];
	dna = "-";
	sizes = "-";
	directions = "-";
	iterations = "-";
	let number = Number(config.step);
  	if (typeof(number) === "number" && Number.isInteger(number)) {
    	step = number;
	} else {
		step = 25;
	}
	number = Number(config.lw);
	if (typeof(number) === "number" && Number.isInteger(number)) {
    	lw = number;
	} else {
		lw = 3;
	}
	let string = config.size;
	if (typeof(string) === "string" && Number.isInteger(Number(string))) {
		currentsize = Number(number);
	} else {
		currentsize = 1;
	}
	string = config.info;
	if (typeof(string) === "string" && string === "false") {
    	showinfo = false;
	} else {
		showinfo = true;
	}
	string = config.grid;
	if (typeof(string) === "string" && string === "true") {
    	showgrid = true;
	} else {
		showgrid = false;
	}
	string = config.background;
	bg = color(34);
	if (typeof(string) === "string"){
		values = string.split(",");
		print(values);
		if (values.length === 3){
			rgb = [Number(values[0]), Number(values[1]), Number(values[2])]
			print(rgb);
			if (!(isNaN(rgb[0]) || isNaN(rgb[1] || isNaN(rgb[2])))){
				bg = color(rgb[0], rgb[1], rgb[2]);
			}
		}
	}
	string = config.color;
	c = color(153,170,169);
	if (typeof(string) === "string"){
		values = string.split(",");
		print(values);
		if (values.length === 3){
			rgb = [Number(values[0]), Number(values[1]), Number(values[2])]
			if (!(isNaN(rgb[0]) || isNaN(rgb[1] || isNaN(rgb[2])))){
				c = color(rgb[0], rgb[1], rgb[2]);
			}
		}
	}
	strokeWeight(lw);
	textAlign(TOP, TOP);
	textSize(height / 25);
	textline = height / 25 * 1.1;
}
