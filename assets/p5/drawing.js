//Aquí dejamos las variables globales. ¡No tocar!
let lw, step;
let c, bg;
let thecanvas;
let origin, lastpoint;
let currentsize, currentdirection, wheel;

//Para dibujar una línea.
function dibujar(l){
	stroke(c);
	strokeWeight(lw);
	p2 = getPoint(lastpoint, currentdirection, l);
	line(lastpoint[0],lastpoint[1],p2[0],p2[1]);
	lastpoint = p2;
}

//Para saltar.
function saltar(l){
	p2 = getPoint(lastpoint, currentdirection, l);
	lastpoint = p2;
}

//Para girar.
function girar(t){
	currentdirection = (currentdirection + t)%8;
}

//La función para dejar lista la hoja.
function prepararHoja(){
  background(bg);
  printGrid();
  lastpoint = origin;
}

//Esta es la función que configura inicialmente el programa.
function startConfig(thestep, linewidth, background, thecolor) {
	origin = [width * 0.33, height * 0.66];
	lastpoint = origin;
	currentdirection = 0;
	wheel = [[0,-1],[1,-1],[1,0],[1,1],[0,1],[-1,1],[-1,0],[-1,-1]];
	step = thestep;
    lw = linewidth;
    currentsize = 1;
    showgrid = true;
	bg = color(background);
	c = color(thecolor);
	strokeWeight(lw);
}

//Esta función calcula el próximo punto al que tiene que llegar una línea.
function getPoint(p, d, l){
	p2 = [];
	p2[0] = p[0] + wheel[d][0] * step * l;
	p2[1] = p[1] + wheel[d][1] * step * l;
	return p2;
}

//Esta función pinta los cuadraditos.
function printGrid(){
	stroke(c);
	strokeWeight(1);
	line(origin[0],0,origin[0],height);
	let x = origin[0] + step;
	while (x < width){
		line(x,0,x,height);
		x += step;
	}
	x = origin[0] - step;
	while (x > 0){
		line(x,0,x,height);
		x -= step;
	}
	line(0,origin[1],width,origin[1]);
	let y = origin[1] + step;
	while (y < height){
		line(0,y,width,y);
		y += step;
	}
	y = origin[1] - step;
	while (y > 0){
		line(0,y,width,y);
		y -= step;
	}
	strokeWeight(lw*3);
	point(origin[0],origin[1]);
}

